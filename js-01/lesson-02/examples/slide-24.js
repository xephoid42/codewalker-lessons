
var app = angular.module('myApp', ['ngRoute']);

		app.controller('AppCtrl', function() {});

		app.controller('IndexCtrl', function() {});

		app.controller('AboutCtrl', function() {});

		app.config(['$routeProvider', function($routeProvider) {
			$routeProvider.when('/', {
				templateUrl: 'parts/index.html',
				controller: 'IndexCtrl'
			})
			.when('/about', {
				templateUrl: 'parts/about.html',
				controller: 'AboutCtrl'
			})
			.otherwise({
				redirectTo: '/'
			});
		}]);